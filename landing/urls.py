from django.contrib import admin
from django.urls import path
from . import views

app_name = "landing"

urlpatterns = [
    path('gallery/', views.gallery),
    path('edu/', views.edu),
    path('skills/', views.skills),
    path('', views.home),
]
