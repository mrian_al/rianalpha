from django.http import HttpResponse
from django.shortcuts import render

def home(request):
    return render(request, 'home.html')
    
def edu(request):
    return render(request, 'edu.html')


def skills(request):
    return render(request, 'skills.html')
    

def gallery(request):
    return render(request, 'galeri.html')

def scheduler(request):
    return render(request, 'scheduler.html')
