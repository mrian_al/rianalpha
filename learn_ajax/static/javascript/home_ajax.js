jQuery(document).ready(function($){

    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=indonesia",
        success : function(result){
            for (i = 0; i < result.items.length; i++){
                var numb = i + 1;
                var img_source = "https://static.thenounproject.com/png/340719-200.png";

                var author = "unknown"; 

                if (result.items[i].volumeInfo.authors){
                    author = result.items[i].volumeInfo.authors[0];

                }

                if (result.items[i].volumeInfo.imageLinks){
                    if (result.items[i].volumeInfo.imageLinks.thumbnail){
                        img_source = result.items[i].volumeInfo.imageLinks.thumbnail;
                    }else if (result.items[i].volumeInfo.imageLinks.smallThumbnail){
                        img_source = result.items[i].volumeInfo.imageLinks.smallThumbnail;
                    }
                }

                var data = 
                "<tr class='temp'>" +
                    "<th scope='row'>" + numb +  "</th>" +
                    "<td>" + result.items[i].volumeInfo.title + "</td>" +
                    "<td>" + author + "</td>" +
                    "<td> <img src=" + img_source + "></img>" + 
                    "</td>" +
                "</tr>";

                $("#body_table").append(data);
            }
        }
    })                

    
    $("#search_button").click(function(){
        $(".temp").remove();
        var the_keyword = document.getElementById('user_input').value;
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + the_keyword,
            success : function(result){
                for (i = 0; i < result.items.length; i++){
                    var numb = i + 1;
                    var img_source = "https://static.thenounproject.com/png/340719-200.png";

                    var author = "unknown"; 

                    if (result.items[i].volumeInfo.authors){
                        author = result.items[i].volumeInfo.authors[0];

                    }

                    if (result.items[i].volumeInfo.imageLinks){
                        if (result.items[i].volumeInfo.imageLinks.thumbnail){
                            img_source = result.items[i].volumeInfo.imageLinks.thumbnail;
                        }else if (result.items[i].volumeInfo.imageLinks.smallThumbnail){
                            img_source = result.items[i].volumeInfo.imageLinks.smallThumbnail;
                        }
                    }



                    var data = 
                    "<tr class='temp'>" +
                        "<th scope='row'>" + numb +  "</th>" +
                        "<td>" + result.items[i].volumeInfo.title + "</td>" +
                        "<td>" + author + "</td>" +
                        "<td> <img src=" + img_source + "></img>" + 
                        "</td>" +
                    "</tr>";

                    $("#body_table").append(data);
                }

            }
        })                

    });

});