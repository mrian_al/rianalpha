from django.apps import AppConfig


class LearnAjaxConfig(AppConfig):
    name = 'learn_ajax'
