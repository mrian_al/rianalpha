from django.urls import path
from .views import home_ajax

app_name = "learn_ajax"

urlpatterns = [
    path('', home_ajax),
]
