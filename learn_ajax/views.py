from django.shortcuts import render

# Create your views here.

def home_ajax(request):
    return render(request, 'home_ajax.html')
