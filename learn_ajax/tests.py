from django.test import TestCase
from django.test import Client
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys

# Create your tests here.

class TestAjaxPage(TestCase):
    
    response = Client().get("/learnajax/")
    def test_url_is_exist(self):
        self.assertEqual(self.response.status_code, 200)
    
    def test_contains_some_string(self):
        self.assertContains(self.response, "FIND YOUR FAVORITE BOOK HERE")

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome("./chromedriver", chrome_options = chrome_options)

    def test_ajax_can_get_data(self):
        driver = self.browser
        driver.get('http://localhost:8000/learnajax')    
        time.sleep(1)
        element = driver.find_element_by_id("user_input")
        element.send_keys("Indonesia")
        submit = driver.find_element_by_id("search_button")
        submit.send_keys(Keys.RETURN)
        time.sleep(3)
        assert "Indonesia" in driver.page_source
    
    def tearDown(self):
        self.browser.quit()