from django.urls import path
from .views import login_page, success_page, logout_account

app_name = "learn_auth"

urlpatterns = [
    path('/', login_page, name="login_page"),
    path('/success/', success_page, name="success_page"),
    path('/success_logout', logout_account, name='logout_user')
]