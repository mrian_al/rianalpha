from django.shortcuts import render
from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import AuthenticationForm,UserCreationForm
from django.contrib.auth.decorators import login_required



def login_page(request):
    if request.user.is_authenticated:
        return redirect('learn_auth:success_page')
    
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request,user)
            request.session["user"] = request.POST["username"]
            return redirect("learn_auth:success_page")
    
    form = AuthenticationForm()
    return render(request, "index_auth.html", {"form": form })

@login_required(login_url="learn_auth:login_page")
def success_page(request):
    username = request.session.get("user")
    return render(request, "success.html", {"username" : username})


def logout_account(request):
    if request.method == "POST":
        logout(request)
    return redirect("learn_auth:login_page")
