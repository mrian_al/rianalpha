from django.test import TestCase,Client
from django.urls import resolve
from . import views
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import unittest, time
from selenium.webdriver.common.keys import Keys
from datetime import date, time
from time import sleep
from django.test import LiveServerTestCase

class TestAuth(TestCase):
    def test_access_auth_page(self):
        response = Client().get("/learnauth/")
        self.assertEqual(response.status_code, 200)
    
    def test_auth_page_views(self):
        response = resolve("/learnauth/")
        self.assertEqual(response.func, views.login_page)

    def test_auth_page_template(self):
        response = Client().get("/learnauth/")
        self.assertTemplateUsed(response, "index_auth.html")