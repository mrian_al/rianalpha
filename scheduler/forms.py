from django import forms
from scheduler import models


class Scheduler(forms.Form):
    activity = forms.CharField(
        label='activity',  widget=forms.TextInput(attrs={
        'class' : "form-control",
        'type' : "text",
        'aria-label' : "Default",
        'aria-describedby' : "inputGroup-sizing-default",
        }), max_length=50)

    place = forms.CharField(label='place', widget=forms.TextInput(attrs={
        'class' : "form-control",
        'type' : "text",
        'aria-label' : "Default",
        'aria-describedby' : "inputGroup-sizing-default",
        }))

    category = forms.ChoiceField(
        label='category', 
        choices=[('Akademik', 'Akademik'),
            ('Personal', 'Personal'),
            ('Keluarga', 'Keluarga'),
            ('Lainnya', 'Lainnya')],
        widget=forms.Select(attrs={
        'class' : "form-control",
        'type' : "text",
        'aria-label' : "Default",
        'aria-describedby' : "inputGroup-sizing-default",
        }))

    date = forms.DateField(label="date", widget=forms.DateInput(attrs={
        'class' : "form-control",
        'type' : "date",
        'aria-label' : "Default",
        'aria-describedby' : "inputGroup-sizing-default",
        }))
    
    time = forms.TimeField(label="time", widget=forms.TimeInput(attrs={
        'class' : "form-control",
        'type' : "time",
        'aria-label' : "Default",
        'aria-describedby' : "inputGroup-sizing-default",
        }))
    
    class Meta:
        model = models.Activities
        fields = ['activity', 'place', 'category', 'date', 'time']
