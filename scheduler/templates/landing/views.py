from django.shortcuts import render, redirect
from .models import Product
from .forms import MakeProduct


def see_desc(request, id):
    arg = Product.objects.get(pk=id)
    context = {
        "barang" : arg,
        "page_title" : arg.name
    }
    return render(request, "landing/desc.html", context)

def index(request):

    all_product = Product.objects.all().order_by("-id")
    arguments = {
        "page_title" : "Welcome to Your Pets",
        "products" : all_product,
        "top3" : all_product[:3:],
    }
    return render(request, "landing/landing_page.html", arguments)

def add_product(request):
    a_form = MakeProduct(request.POST or None, request.FILES or None)
    
    arguments = {
        "page_title" : "Add a Product",
        "form" : a_form,
    }

    if request.method == "POST" :
        if a_form.is_valid():
            a_form.save()
            return redirect("landing:add_my_product")
        else:
            a_form = MakeProduct()

    return render(request,"landing/addproduct.html", arguments)