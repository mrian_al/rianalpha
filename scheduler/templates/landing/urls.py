from django.urls import path, include
from landing.views import index
from landing.views import add_product
from landing.views import see_desc
from django.conf import settings
from django.conf.urls.static import static

app_name = "landing"

urlpatterns = [
    path("", index, name='index'),
    path("addproduct/", add_product, name='add_my_product'),
    path("desc/<int:id>/", see_desc, name="description")
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)