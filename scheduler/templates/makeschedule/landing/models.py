from django.db import models

class Product(models.Model):
    name = models.TextField()
    imgProduct = models.ImageField(upload_to = "images/")
    price = models.IntegerField()
    category = models.TextField()
    description = models.TextField()