from django import forms
from .models import Product

class MakeProduct(forms.ModelForm):
    
    name = forms.CharField(
        label = "name", widget = forms.TextInput(attrs={
            "class" : "form-control",
            "type" : "text",
            "aria-label" : "Default",
            "aria-describedby" : "inputGroup-sizing-default"
        }),

    )

    imgProduct = forms.ImageField(
        label = "imgProduct",
        widget = forms.FileInput(attrs={
            "class" : "form-control",
            "type" : "file",
            "aria-label" : "Default",
            "aria-describedby" : "inputGroup-sizing-default",            
        })
    )

    category = forms.ChoiceField(
        label = "category", 
        
        choices = [
            ("Pets", "Pets"),
            ("Toys", "Toys"),
            ("Foods", "Foods"),
            ("Others", "Others"),
            ],

        widget = forms.Select(attrs = {
            "class" : "form-control",
            "type" : "text",
            "aria-label" : "Default",
            "aria-describedby" : "inputGroup-sizing-default",
        })
    )

    price = forms.IntegerField(
        label = "price",
        widget = forms.NumberInput(attrs={
            "class" : "form-control",
            "type" : "text",
            "aria-label" : "Default",
            "aria-describedby" : "inputGroup-sizing-default",

        })
    )

    description = forms.CharField(
        label = "description", widget = forms.Textarea(attrs={
            "class" : "form-control",
            "type" : "text",
            "aria-label" : "Default",
            "aria-describedby" : "inputGroup-sizing-default",
        })
    )
   
    class Meta:
        model = Product
        fields = ["name", "price", "imgProduct" , "category", "description"]
