from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Activities(models.Model):

    activity = models.CharField(max_length=100)
    date = models.DateField()
    time = models.TimeField()
    place = models.TextField()
    category = models.TextField()
    