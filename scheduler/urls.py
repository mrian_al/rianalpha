from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = 'scheduler'

urlpatterns = [
    path('', views.make_schedule, name='make_schedule'),
    path('delete/<int:id>/', views.delete, name="delete"),
]

urlpatterns += staticfiles_urlpatterns()