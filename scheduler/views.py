from django.shortcuts import render, redirect
from .models import Activities
from . import forms
from .forms import Scheduler
from datetime import datetime


def make_schedule(request):
    form = Scheduler(request.POST)

    if request.method == "POST":
        if form.is_valid():
            new = Activities(
                activity = form.data['activity'],
                place = form.data['place'],
                date = form.data['date'],
                time = form.data['time'],
                category = form.data['category'],
                )
            new.save()
            return redirect('scheduler:make_schedule')
        else :
            form = form.Scheduler()
    schedule = Activities.objects.all().values()
    argument = {
        'schedules_active' : 'active',
        'context' : {'now': datetime.now()},
        'jadwalgue': schedule,
        'form' : form,

    }
    return render(request, 'makeschedule/scheduler.html', argument)

def delete(request, id):   
    Activities.objects.get(pk=id).delete()
    return redirect('scheduler:make_schedule')
